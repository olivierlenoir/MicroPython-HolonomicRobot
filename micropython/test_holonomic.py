# Author: Olivier Lenoir - <olivier.len02@gmail.com>
# Created: 2021-06-23 18:57:54
# Updated:
# License: MIT, Copyright (c) 2021 Olivier Lenoir
# Language: MicroPython v1.16
# Project: Holonomic Robot
# Device: ESP32, stepper motor driver DRV8825
# Description: First test
# Contact: Ducros Christian - Christian.Ducros@ac-strasbourg.fr

from holobot import Wheel, Holobot
from utime import sleep


w0 = Wheel(dir_pin=2, step_pin=4)
w1 = Wheel(dir_pin=19, step_pin=15)
w2 = Wheel(dir_pin=5, step_pin=18)

holobot = Holobot(w0, w1, w2)

holobot.move(10, -20, 50)
sleep(3)
holobot.stop()
print(holobot.cur_pos())
holobot.move(-20, 50, -10)
sleep(5)
holobot.stop()
print(holobot.cur_pos())
holobot.move(10, -20, 50)
holobot.stop()
