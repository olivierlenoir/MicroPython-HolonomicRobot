# Author: Olivier Lenoir - <olivier.len02@gmail.com>
# Created: 2021-06-23 18:58:07
# Updated:
# License: MIT, Copyright (c) 2021 Olivier Lenoir
# Language: MicroPython v1.16
# Project: Holonomic Robot
# Description: ESP32 with DRV8825

from machine import Pin, Timer
from esp32 import RMT

DRV8825_mp = const(100)


class Wheel(object):

    id_wheel = 0
    id_timer = -1

    def __init__(self, dir_pin, step_pin):
        self.d_pin = Pin(dir_pin, Pin.OUT)
        self.s_pin = RMT(Wheel.id_wheel, pin=Pin(step_pin), clock_div=80)
        self.wp = self.s_pin.write_pulses
        Wheel.id_wheel += 1
        self.pos = 0
        # Frequency (Hz)
        self.frequency = 0
        # Init vitual timer
        self.tmr = Timer(self.id_timer)
        Wheel.id_timer -= 1

    def set_dir(self, freq):
        d = self.d_pin()
        if freq < 0 and d:
            self.d_pin(0)
        elif freq > 0 and not d:
            self.d_pin(1)

    def step(self):
        self.wp((DRV8825_mp, DRV8825_mp))
        self.pos += 1 if self.d_pin() else -1

    def freq(self, freq=None):
        if freq is None:
            return self.frequency
        elif freq == 0:
            self.tmr.deinit()
        elif freq != self.frequency:
            self.set_dir(freq)
            self.frequency = freq
            self.tmr.init(
                freq=abs(self.frequency),
                callback=lambda t: self.step()
                )


class Holobot(object):

    def __init__(self, *wheel):
        self.wheels = wheel

    def cur_pos(self):
        return tuple(w.pos for w in self.wheels)

    def move(self, *freqs):
        for w, f in zip(self.wheels, freqs):
            w.freq(f)

    def stop(self):
        for w in self.wheels:
            w.freq(0)
